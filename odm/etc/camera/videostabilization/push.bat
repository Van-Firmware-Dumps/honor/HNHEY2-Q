adb remount

adb shell "mkdir -p /odm/etc/camera/videostabilization"
adb push param_control.xml /odm/etc/camera/videostabilization/
adb push param_algo.xml /odm/etc/camera/videostabilization/
adb push param_algo_B.xml /odm/etc/camera/videostabilization/
adb push param_pq.xml /odm/etc/camera/videostabilization/
adb push tnr_parameter.xml /odm/etc/camera/videostabilization/
adb push gpuwarp.bin /odm/etc/camera/videostabilization/

pause

adb reboot